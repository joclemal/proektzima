<!DOCTYPE html>
<html>
<head>
	<title>websitemaker</title>
	<meta charset="utf-8">
	<style type="text/css">
		body{
			background-color: #676bef;
		}
		div{
			margin:15px;
		}
		input[type=text] {
    		width: 40%;
    		height: 10%;
     		margin: 8px 0;
    	}
    	.mar{
    		margin:10px;
    		padding: 40px;
    	}
    	.pad{
    		padding-top: 30px;
    	}
    	input[type="submit"]{
			color: white;
			background-color: #14489b;
			border-radius: 20px;
			border:1px solid #14489b;
			padding:10px 20px;	
		}

	</style>
</head>
<body>
	<center>
		<h2>Еден чекор ве дели од вашата веб страна</h2>
		<form action="baza.php" method="POST" accept-charset="utf-8">
		<div> 
		<b><label>Име на компанијата</label></b><br>
		<input type="text" name="ime_na_komp" width="160" height="5"><br>
		</div>
		<div> 
		<b><label>Напишете го линкот до cover сликата</label></b><br>
		<input type="text" name="cover_slika" size="60"><br>
		</div>
		<div> 
		<b><label>Внесете го насловот</label></b><br>
		<input type="text" name="naslov" size="60"><br>
		</div>
		<div> 
		<b><label>Внесете го поднасловот</label></b><br>
		<input type="text" name="podnaslov" size="60"><br>
		</div>
		<div>
		<b><label>Напишете нешто за вас</label></b><br>
		<textarea name="nestozavas" rows="10" cols="60"></textarea>	
		</div>
		<b><label>Внесете го вашиот телефон</label></b><br>
		<input type="text" name="telefon" size="60"><br>
		</div>
		<b><label>Внесете ја вашата локација</label></b><br>
		<input type="text" name="lokacija" size="60"><br>
		</div>
		<hr/>
		<div>
		<b><label>Одберете дали нудите сервиси или продукти</label></b><br>
		<select name="service">
			<option value="servisi">Сервиси</option>
			<option value="produkti">Продукти</option>
		</select>
		</div>
		<b><p>Внесете URL од слика и опис на вашите продукти или сервиси</p></b><br>
		<div>
		<b><label>URL од слика</label></b><br>
		<input type="text" name="url_slika1" size="60"><br>
		<b><label>Опис за сликата</label></b><br>
		<textarea name="opiszaslika1" rows="10" cols="60"></textarea>	
		</div>
		<div>
		<b><label>URL од слика</label></b><br>
		<input type="text" name="url_slika2" size="60"><br>
		<b><label>Опис за сликата</label></b><br>
		<textarea name="opiszaslika2" rows="10" cols="60"></textarea>	
		</div>
		<div>
		<b><label>URL од слика</label></b><br>
		<input type="text" name="url_slika3" size="60"><br>
		<b><label>Опис за сликата</label></b><br>
		<textarea name="opiszaslika3" rows="10" cols="60"></textarea>	
		</div>
		<hr/>
		<div>
		<div class="pad"><b><label>Напишете нешто за вашата фирма што луѓето треба да го знаат пред да ве контактираат</label></b><br></div>
		<textarea name="za_vas" rows="10" cols="60"></textarea>	
		</div>
		<div>
		<b><label>Напишете нешто што сакате да го пишува најдоле на страната</label></b><br>
		<textarea name="najdole_na_stranata" rows="10" cols="60"></textarea>	
		</div>
		<div class="mar"><b><label>Внесете ги линковите од вашите социјални мрежи</label></b><br></div>
		<div>
		<b><label>LinkedIn</label></b><br>
		<input type="text" name="linkedin" size="60"><br>
		<b><label>Facebook</label></b><br>
		<input type="text" name="facebook" size="60"><br>
		<b><label>Twitter</label></b><br>
		<input type="text" name="twitter" size="60"><br>
		<b><label>Google+</label></b><br>
		<input type="text" name="google" size="60"><br>
		<br>
		<input type="submit" value="Потврди">
		</form>
	</center>
</body>
</html>	