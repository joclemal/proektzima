<?php

$id = $_GET['id'];

$username = 'root';
$password = ' ';
$database_host = 'localhost';
$database_name = 'proektzima';
$database_type = 'mysql';

$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);

$statement = $connection->prepare('SELECT * FROM proekt WHERE id = :id');

$statement->bindValue(':id', $id);

$statement->execute();

$result = $statement->fetch(PDO::FETCH_ASSOC);


?>


<!DOCTYPE html>
<html>
<head>
	<title>websitemaker</title>
	<meta charset="utf-8">

	<link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
	<div class="navbar">
		<ul>
				<a href="#" ><li>ДОМА</li></a>
				<a href="#zanas"><li>ЗА НАС</li></a>
				<a href="#silip"><li><?php if($result['service']='servisi'){echo 'СЕРВИСИ';}else{echo 'ПРОИЗВОДИ';} ?></li></a>
				<a href="#kontakti"><li>КОНТАКТ</li></a>
		</ul>
	</div>
	<div class="slika" style="background-size:cover; background-image: url(' <?php echo $result['cover_slika']?>')">
		<!-- <img src="../cover.jpg" alt="ova"> -->
		<center>
		<div class="naslov">
			<h1><?php echo $result['naslov']?></h1>
			<h5><?php echo $result['podnaslov']?></h5>
		</div>
	</center>
	</div>

	
	
	<div class="zanas">
		<b><p id="zanas">За нас</p></b>
		<p><?php echo $result['nestozavas']?></p>
	</div>
	<div class="tel">
		<b><p>Телефон</p></b>
		<p><?php echo $result['telefon']?></p><br>
		<b><p>Локација</p></b>
		<p><?php echo $result['lokacija']?></p>
	</div>
	<center>
		<div class="silip" id="silip"><b><p><?php if($result['service']='servisi'){echo 'Сервиси';}else{echo 'Производи';} ?></p></b></div>
	</center>
<div class="prazen">
	<div class="sliki">
		<img src="<?php echo $result['url_slika1']?>" alt="Error loading image" width="100%">
	</div>
	<div class="sliki">
		<img src="<?php echo $result['url_slika2']?>" alt="Error loading image" width="100%">
	</div>
	<div class="sliki">
		<img src="<?php echo $result['url_slika3']?>" alt="Error loading image" width="100%">
	</div>
</div>


	<div class="produkti">
		<h4>Опис за првиот продукт</h4>
		<p><?php echo $result['opiszaslika1']?><p>	
	</div>
	<div class="produkti">
		<h4>Опис за првиот продукт</h4>
		<p><?php echo $result['opiszaslika2']?><p>	
	</div>
	<div class="produkti">
		<h4>Опис за првиот продукт</h4>
		<p><?php echo $result['opiszaslika3']?><p>
	</div>


	<center>
		<div class="kontakti" id="kontakti"><b><p>Контакти</p></b></div>
	</center>	

	
</div>
	<div class="tekst">
		<b><p>Текст</p></b>
		<p><?php echo $result['za_vas']?></p>
	</div>


<div class="forma">
	<center>
		<div class="textforma">
	<form>
			<label style="float:left; margin-right: 30px;">Име</label>
			<input type="text" placeholder="Вашето име" name=""><br>
			<label style="float:left; margin-right: 10px;">Емаил</label>
			<input type="text" placeholder="Вашиот емаил" name=""><br>
			<label style="float:left; margin-right: 5px; margin-top: 25px;">Порака</label>
			<textarea placeholder="Вашата порака" name="" rows="5" cols="33"></textarea><br><br>
			<input type="button" value="Испрати">
		</div>
	</center>
</div>
<div class="bardole">
	<div class="text_bardole">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco.</p>
	</div>
	<div class='logoa'>
		<a href="<?php echo $result['linkedin']?>" ><img src="linkedin.png" alt="ova"></a>
		<a href="<?php echo $result['facebook']?>" ><img src="facebook.png" alt="ova">
		<a href="<?php echo $result['twitter']?>" ><img src="twitter.png" alt="ova">
		<a href="<?php echo $result['google']?>" ><img src="google.png" alt="ova">
	</div>


</div>

</body>
</html>	