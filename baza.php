<?php

error_reporting(E_ALL);
ini_set('display_errors','1');

$username = 'root';
$password = ' ';
$database_host = 'localhost';
$database_name = 'proektzima';
$database_type = 'mysql';

try
{

// var_dump($_POST);

$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);

$statement = $connection->prepare('INSERT INTO proekt (ime_na_komp, cover_slika, naslov,
podnaslov, nestozavas, telefon, lokacija, service, url_slika1, opiszaslika1, url_slika2, 
opiszaslika2, url_slika3, opiszaslika3, za_vas, najdole_na_stranata, linkedin, facebook, twitter, google) VALUES (:ime_na_komp, :cover_slika, :naslov,
:podnaslov, :nestozavas, :telefon, :lokacija, :service, :url_slika1, :opiszaslika1, :url_slika2, 
:opiszaslika2, :url_slika3, :opiszaslika3, :za_vas, :najdole_na_stranata, :linkedin, :facebook, :twitter, :google)');

$statement->bindParam(':ime_na_komp', $_POST['ime_na_komp']);
$statement->bindParam(':cover_slika', $_POST['cover_slika']);
$statement->bindParam(':naslov', $_POST['naslov']);
$statement->bindParam(':podnaslov', $_POST['podnaslov']);
$statement->bindParam(':nestozavas', $_POST['nestozavas']);
$statement->bindParam(':telefon', $_POST['telefon']);
$statement->bindParam(':lokacija', $_POST['lokacija']);
$statement->bindParam(':service', $_POST['service']);
$statement->bindParam(':url_slika1', $_POST['url_slika1']);
$statement->bindParam(':opiszaslika1', $_POST['opiszaslika1']);
$statement->bindParam(':url_slika2', $_POST['url_slika2']);
$statement->bindParam(':opiszaslika2', $_POST['opiszaslika2']);
$statement->bindParam(':url_slika3', $_POST['url_slika3']);
$statement->bindParam(':opiszaslika3', $_POST['opiszaslika3']);
$statement->bindParam(':za_vas', $_POST['za_vas']);
$statement->bindParam(':najdole_na_stranata', $_POST['najdole_na_stranata']);
$statement->bindParam(':linkedin', $_POST['linkedin']);
$statement->bindParam(':facebook', $_POST['facebook']);
$statement->bindParam(':twitter', $_POST['twitter']);
$statement->bindParam(':google', $_POST['google']);


$result = $statement->execute();

// $result = $statement->fetchALL(PDO::FETCH_ASSOC);

$id = $connection->lastInsertId();

header('Location: treta.php?id='.$id);


}
catch(PDOException $e){
	var_dump($e);
}
